
// [SECTION] - While loop

let count = 5;

// While the value of count is not equal to 0
while(count !==0){
	//first iteration --> count == 5
	//Second iteration --> count == 4
	//Third iteration --> count == 3
	//Fourth iteration --> count == 2
	//Fifth Iteration --> count == 1

	console.log("While: " + count);
	count--;
}

// [SECTION] - Do While Loop
/*
Syntax:
do{
	//code block
}while(expression/condition)

*/

/*let number = Number(prompt("Give me a number"));

do{
	// The current value of number is printed out.
	console.log("Do While " + number)

	//increases the value of number by 1 after every iteration/loop to stop the loop when it reaches to 10
	number +=1;
}while(number <= 10)
*/
// [SECTION] - For Loops
/*
SYNTAX:
for(initialization; condition; final expression/iteration){
	//code blocks
}

*/

/*for(let count = 0; count <= 20; count++){
	console.log("For Loop: " + count)
}*/
let myString = "alex";

//.length() -> characters in strings may be counted using the property
// Strings are specials compared to other data types in that access to functions and other pieces of information
console.log(myString.length)

// Accessing elements of a string 
// Individual Characters

console.log(myString[0]);
console.log(myString[1]);
console.log(myString[2]);
console.log(myString[3]);

// Will create a loop that will print out the individual letters of myString variable.

/*for(let x = 0; x < myString.length; x++){
	// The current value of myString is printed out using its index

	console.log(myString[x]);
}*/

// Create a string named "myName"
let myName = "ALEx"

for(let i = 0; i < myName.length; i++){
	//console.log(myName[i].toLowerCase());
	
	if(
		myName[i].toLowerCase() == "a" || 
		myName[i].toLowerCase() == "i" || 
		myName[i].toLowerCase() == "o" || 
		myName[i].toLowerCase() == "u" ||
		myName[i].toLowerCase() == "e"   
		) {
		//If the letter in the name is a vowel, it will print number 3
		console.log(3);
	}else{
		//print in the console all non-vowel characters in the name
		console.log(myName[i]);
	}
}

// [SECTION] - Continue and Break Statements

/*for(let count = 0; count <= 20; count++){
	// if remainder is equal to 0
	if(count % 2 === 0){
		continue;
	}


	console.log("Continue and Break: " + count);
	// If the current value of count is greater than 10 the flow will stop
	if(count > 10) {
		break;
	}
}
*/
let name = "alexandro"

for(let i = 0; i< name.length; i++){
	// will print current letters based on its index
	console.log(name[i]);

	//If the vowel is equal to a, continue the next iteration of the loop
	if(name[i].toLowerCase() === "a"){
		console.log("Continue to the next Iteration");
		continue;
	}

	// if the current letter is equal to d, stop the loop
	if(name[i] === "d"){
		break;
	}
	}
